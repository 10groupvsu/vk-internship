<?php

require_once __DIR__ . '/../models/User.php'; // Подключаем модель User
require_once __DIR__ . '/../models/Quest.php'; // Подключаем модель Quest

//Класс ApiController
class ApiController {
    private $userModel;
    private $questModel;

    public function __construct($db) {
        $this->userModel = new User($db);
        $this->questModel = new Quest($db);
    }

    // Метод для создания нового пользователя (User)
    public function createUser() {
        // Проверяем, были ли переданы данные
        if (isset($_POST['name']) && isset($_POST['balance'])) {
            // Получаем данные из запроса
            $name = $_POST['name'];
            $balance = $_POST['balance'];

            // Вызываем метод модели для создания нового пользователя
            $user = $this->userModel->createUser($name, $balance);

            // Возвращаем результат в формате JSON
            echo json_encode($user, JSON_UNESCAPED_UNICODE);
        } else {
            // Если данные не были переданы, вы можете вывести ошибку или перенаправить пользователя
            echo json_encode(["error" => "Name and balance are required."]);
        }
    }

    // Метод для создания задания (Quest)
    public function createQuest() {
        // Проверяем, были ли переданы данные
        if (isset($_POST['name']) && isset($_POST['cost'])) {
            // Получаем данные из запроса
            $name = $_POST['name'];
            $cost = $_POST['cost'];

            // Вызываем метод создания задания
            $result = $this->questModel->createQuest($name, $cost);

            // Возвращаем результат в формате JSON
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            // Если данные не были переданы, вы можете вывести ошибку или перенаправить пользователя
            echo json_encode(["error" => "Name and cost are required."]);
        }
    }

    // Метод для завершения задания
    public function completeQuest() {
        // Проверяем, были ли переданы данные
        if (isset($_POST['user_id']) && isset($_POST['quest_id'])) {
            // Получаем данные из запроса
            $user_id = $_POST['user_id'];
            $quest_id = $_POST['quest_id'];

            // Вызываем метод модели для завершения задания
            $result = $this->userModel->completeQuest($user_id, $quest_id);

            // Возвращаем результат в формате JSON
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            // Если данные не были переданы, вы можете вывести ошибку или перенаправить пользователя
            echo json_encode(["error" => "User ID and quest ID are required."]);
        }
    }

    // Метод получения истории выполненных заданий и баланса пользователя
    public function getUserHistory() {
        // Проверяем, были ли переданы данные
        if (isset($_GET['user_id'])) {
            // Получаем данные из запроса
            $user_id = $_GET['user_id'];

            // Вызываем метод модели для получения истории выполненных заданий и баланса пользователя
            $history = $this->userModel->getUserHistory($user_id);

            // Возвращаем результат в формате JSON
            echo json_encode($history, JSON_UNESCAPED_UNICODE);
        } else {
            // Если данные не были переданы, вы можете вывести ошибку или перенаправить пользователя
            echo json_encode(["error" => "User ID is required."]);
        }
    }
}