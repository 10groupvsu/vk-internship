<?php

// Параметры подключения к базе данных
$host = '127.0.0.1'; // Хост
$dbname = 'internship'; // Имя базы данных
$username = 'root'; // Имя пользователя базы данных
$password = 'root'; // Пароль пользователя базы данных

try {
    // Подключение к базе данных
    $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);
    // Установка режима обработки ошибок PDO на исключения
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo $host, $dbname, $username, $password;
    // Обработка ошибок подключения
    echo 'Connection failed: ' . $e->getMessage();
    exit;
}

?>
