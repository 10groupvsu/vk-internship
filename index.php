<?php

require __DIR__ . '/dbconn.php';
require __DIR__ . '/controllers/ApiController.php';

// Создаем экземпляр контроллера
$controller = new ApiController($db);

// Определяем запрашиваемый маршрут
$route = isset($_GET['route']) ? $_GET['route'] : '';

// Маршрутизируем запросы к соответствующим методам контроллера
switch ($route) {
    case 'createUser':
        $controller->createUser();
        break;
    case 'createQuest':
        $controller->createQuest();
        break;
    case 'completeQuest':
        $controller->completeQuest();
        break;
    case 'getUserHistory':
        $controller->getUserHistory();
        break;
    default:
        // Обработка некорректного маршрута или вывод домашней страницы
        echo 'Welcome to the API!';
        break;
}

