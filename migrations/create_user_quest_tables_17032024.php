<?php
// Подключаем файл с параметрами подключения к базе данных
require_once __DIR__ . '/../dbconn.php';

// Создаем соединение с базой данных
$conn = new mysqli($host, $username, $password, $dbname);

// Проверяем соединение
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// SQL запрос для создания таблицы пользователей (User)
$sql = "CREATE TABLE IF NOT EXISTS user (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    balance INT(10) NOT NULL
)";

if ($conn->query($sql) === TRUE) {
    echo "Table user created successfully<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

// SQL запрос для создания таблицы заданий (Quest)
$sql = "CREATE TABLE IF NOT EXISTS quest (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(120) NOT NULL,
    cost INT(10) NOT NULL
)";

if ($conn->query($sql) === TRUE) {
    echo "Table quest created successfully<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

// SQL запрос для создания таблицы связи между пользователями и заданиями (user_quest)
$sql = "CREATE TABLE IF NOT EXISTS user_quest (
    user_id INT(6) UNSIGNED,
    quest_id INT(6) UNSIGNED,
    PRIMARY KEY (user_id, quest_id),
    FOREIGN KEY (user_id) REFERENCES User(id),
    FOREIGN KEY (quest_id) REFERENCES Quest(id)
)";

if ($conn->query($sql) === TRUE) {
    echo "Table user_quest created successfully<br>";
} else {
    echo "Error creating table: " . $conn->error;
}

// Закрываем соединение с базой данных
$conn->close();
?>
