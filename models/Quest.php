<?php
// Класс модели Quest
class Quest {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    // Метод для создания нового задания
    public function createQuest($name, $cost) {
        // Подготовка запроса
        $insertQuery = "INSERT INTO quest (name, cost) VALUES (:name, :cost)";
        $insertStmt = $this->db->prepare($insertQuery);

        // Привязка параметров
        $insertStmt->bindParam(":name", $name);
        $insertStmt->bindParam(":cost", $cost);

        // Выполнение запроса
        if ($insertStmt->execute()) {
            // Получение ID только что созданного задания
            $quest_id = $this->db->lastInsertId();

            // Получение информации о созданном задании
            $selectQuery = "SELECT * FROM quest WHERE id = :quest_id";
            $selectStmt = $this->db->prepare($selectQuery);
            $selectStmt->bindParam(":quest_id", $quest_id);
            $selectStmt->execute();

            //Получение информации о созданном задании
            $quest = $selectStmt->fetch(PDO::FETCH_ASSOC);

            // Возвращаем данные о новом задании
            return $quest;
        } else {
            // Если возникла ошибка при выполнении запроса, возвращаем NULL
            return null;
        }
    }
}
?>
