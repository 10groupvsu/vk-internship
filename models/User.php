<?php
// Класс модели User
class User {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    // Метод для создания нового пользователя
    public function createUser($name, $balance) {
        // Подготовка запроса для вставки нового пользователя
        $insertQuery = "INSERT INTO user (name, balance) VALUES (:name, :balance)";
        $insertStmt = $this->db->prepare($insertQuery);

        // Привязка параметров для вставки
        $insertStmt->bindParam(":name", $name);
        $insertStmt->bindParam(":balance", $balance);

        // Выполнение запроса на вставку
        if ($insertStmt->execute()) {
            // Получение ID только что созданного пользователя
            $user_id = $this->db->lastInsertId();
            
            // Подготовка запроса для получения информации о созданном пользователе
            $selectQuery = "SELECT * FROM user WHERE id = :user_id";
            $selectStmt = $this->db->prepare($selectQuery);
            $selectStmt->bindParam(":user_id", $user_id);
            $selectStmt->execute();
            
            // Получение данных о новом пользователе
            $user = $selectStmt->fetch(PDO::FETCH_ASSOC);

            // Возвращаем данные о новом пользователе
            return $user;
        } else {
            // Если возникла ошибка при выполнении запроса на вставку, возвращаем NULL
            return null;
        }
    }

    // Метод завершения задания
    public function completeQuest($user_id, $quest_id) {
        // Проверяем, выполнял ли пользователь уже это задание
        $query = "SELECT * FROM user_quest WHERE user_id = :user_id AND quest_id = :quest_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":user_id", $user_id);
        $stmt->bindParam(":quest_id", $quest_id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        // Если пользователь еще не выполнял это задание
        if (!$result) {
            // Помечаем задание как выполненное пользователем
            $query = "INSERT INTO user_quest (user_id, quest_id) VALUES (:user_id, :quest_id)";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(":user_id", $user_id);
            $stmt->bindParam(":quest_id", $quest_id);
            $stmt->execute();

            // Получаем стоимость задания
            $query = "SELECT cost FROM quest WHERE id = :quest_id";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(":quest_id", $quest_id);
            $stmt->execute();
            $cost = $stmt->fetch(PDO::FETCH_COLUMN);

            // Начисляем награду пользователю
            $query = "UPDATE user SET balance = balance + :cost WHERE id = :user_id";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(":cost", $cost);
            $stmt->bindParam(":user_id", $user_id);
            $stmt->execute();

            // Возвращаем сообщение об успешном завершении задания
            return "Quest completed successfully and reward added to user balance.";
        } else {
            // Если пользователь уже выполнил это задание, возвращаем соответствующее сообщение
            return "User has already completed this quest.";
        }
    }

    // Метод получения истории выполненных заданий и баланса пользователя
    public function getUserHistory($user_id) {
        // Получаем историю выполненных заданий пользователем
        $query = "SELECT q.name AS quest_name, q.cost
                    FROM quest q
                    INNER JOIN user_quest uq ON q.id = uq.quest_id
                    WHERE uq.user_id = :user_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":user_id", $user_id);
        $stmt->execute();
        $user_history = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Получаем текущий баланс пользователя
        $query = "SELECT balance FROM user WHERE id = :user_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":user_id", $user_id);
        $stmt->execute();
        $balance = $stmt->fetch(PDO::FETCH_COLUMN);

        // Собираем данные истории выполненных заданий и текущего баланса вместе
        $result = array(
            "user_id" => $user_id,
            "balance" => $balance,
            "completed_quests" => $user_history
        );

        // Возвращаем историю выполненных заданий и текущий баланс пользователя
        return $result;
    }
}
?>
